import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ovm",
    version="0.0.1",
    author="Knut Karevoll",
    author_email="gnonthgol@gmail.com",
    description="Library to access the Oracle VM API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/gnonthgol/ovmpy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
